#include "dataFunctions.hpp"
 
 
void readData(std::unordered_map<int, BankData>& bankMap, std::ifstream& infile)
{
    int readCount = 0;
 
    double newSavingsWhole = 0, newCheckingWhole = 0, newSavingsDecimal = 0, newCheckingDecimal = 0;
 
    while (!infile.eof())
    {
        std::string newData;
 
        BankData newBankAccount;
 
        if (readCount == 0)
        {
            getline(infile, newData);
        }
 
        getline(infile, newData, ',');
 
        if (newData == "")
        {
            getline(infile, newData, ',');
        }
 
        if (newData == "")
        {
            break;
        }
 
        newBankAccount.setAcctNum(stoi(newData));
 
        getline(infile, newData, '.');
 
        newSavingsWhole = stoi(newData);
 
        getline(infile, newData, ',');
 
        newSavingsDecimal = stoi(newData);
 
        newSavingsWhole += newSavingsDecimal / 100;
 
        newBankAccount.setSavingsAmount(newSavingsWhole);
 
        getline(infile, newData, '.');
 
        newCheckingWhole = stoi(newData);
 
        getline(infile, newData);
 
        newCheckingDecimal = stoi(newData);
 
        newCheckingWhole += newCheckingDecimal / 100;
 
        newBankAccount.setCheckingAmount(newCheckingWhole);
 
        insertData(bankMap, newBankAccount, 1);
 
        readCount++;
    }
 
    std::cout << std::endl;
 
    printNumBuckets(bankMap);
 
    std::cout << std::endl;
 
    printMaxElements(bankMap);
}
 
void insertData(std::unordered_map<int, BankData>& bankMap, BankData& newData, bool printBucket)
{
    std::pair<int, BankData> insertPair;
 
    insertPair = { newData.getAcctNum(), newData };
 
    bankMap.insert(insertPair);
 
    if (printBucket)
    {
        printBucketNumber(bankMap, newData);
    }
}
 
void printData(std::unordered_map<int, BankData> bankMap)
{
    std::unordered_map<int, BankData>::iterator it = bankMap.begin();
 
    while (it != bankMap.end())
    {
        std::cout << it->second << " (Bucket number " << bankMap.bucket(it->first) << ")" << std::endl;
 
        it++;
    }
 
    std::cout << std::endl;
}
 
void printBucketNumber(std::unordered_map<int, BankData> bankMap, BankData& newData)
{
    std::cout << "Number of bucket of account " << newData.getAcctNum() << ": " << bankMap.bucket(newData.getAcctNum()) << std::endl;
}
 
void printNumBuckets(std::unordered_map<int, BankData> bankMap)
{
    std::cout << "Number of buckets in the map: " << bankMap.bucket_count() << std::endl;
}
 
void printMaxElements(std::unordered_map<int, BankData> bankMap)
{
    std::cout << "Maximum size of map: " << bankMap.max_size() << std::endl;
}
 
 
