#include "BankData.hpp"
#include <unordered_map>
#include <fstream>
#include <string>
#include <iostream>

void readData(std::unordered_map<int, BankData> &bankMap, std::ifstream &infile);

void insertData(std::unordered_map<int, BankData> &bankMap, BankData &newData, bool printBucket);

void printData(std::unordered_map<int, BankData> bankMap);

void printBucketNumber(std::unordered_map<int, BankData> bankMap, BankData &newData);

void printNumBuckets(std::unordered_map<int, BankData> bankMap);

void printMaxElements(std::unordered_map<int, BankData> bankMap);