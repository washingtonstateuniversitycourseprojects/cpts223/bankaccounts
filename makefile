prog: main.o BankData.o dataFunctions.o
	g++ main.o BankData.o dataFunctions.o -o runProg

main.o: main.cpp BankData.hpp dataFunctions.hpp
	g++ -c -g -Wall -std=c++11 main.cpp

BankData.o: BankData.cpp BankData.hpp
	g++ -c -g -Wall -std=c++11 BankData.cpp

dataFunctions.o: dataFunctions.cpp dataFunctions.hpp
	g++ -c -g -Wall -std=c++11 dataFunctions.cpp

clean:
	-rm *.o

run: @./runProg