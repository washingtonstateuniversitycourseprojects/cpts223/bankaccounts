/*
    Worst-case Big-O for inserting based on key:		Assuming that the table cannot be resized
														and that the hash function is poor, the worst
														case Big-O would be O(n) where n is the
														number of items in the hash table. I'm
														assuming that the hash table is full and the
														hash function results in probing each spot
														in the hash table for a possible place before
														discovering that the table is full.
	
	Worst-case Big-O for retreiving data linear probe:	Assuming that the table is full and that
														the hash function resulted in every item
														being mapped to the same value, the item to
														retrieve may be one place before its mapped
														value, so one would have to linearly probe
														the entire table to find the value, or O(n)
														retrieval time where n is the number of items
														in the table.

	Worst-case Big-O for deleting data chaining:		Assuming that every item in the table was
														mapped to the same value by the hash function,
														every item in the table is in one linked list.
														Worst case, this item is in the middle of the
														list, and assuming the list can be searched from
														front or back, O(n) is worst case for
														deletion if the item must be found. If the item is
														already located, deletion will be only O(1).

	Worst-case Big-O iterating through map:				Each value must be visited to iterate through the
														map, and assuming an iterator will skip empty memory
														in the map, worst case is O(n).

	When and why should we use an unordered map:		An unordered map should be used when you have a
														large amount of data that you want to access easily,
														but with a few restrictions. You should only use an
														unordered map when you have a reliable hash function,
														that is, a function that will handle collisions well
														or collisions are highly unlikely/do not exist.
														Otherwise access, insertion, and deletion time can be
														as bad as a linked list. Also, you should not mind if
														your items aren't sorted. You should also have a
														trait for your items that you know will make a good key.
														Finally, you should beware of resizing, as that can take
														a lot of time, so if you expect to resize often, an
														unordered map may not work.

	Is std::unordered_map a good choice for bank accounts: std::unordered map can be good in that bank accounts
														have one fairly good choice of key, bank account number.
														Each number is the same length and guaranteed to be
														unique, which is also good. There are not many reasons
														to sort bank accounts, so an unordered map also works in
														that reguard. An unordered map would be a bad choice in
														that new bank accounts are created all the time and
														resizing the map can take a lot of time because items have
														to be re-hashed. Overall, so long as the hash function is
														good, an std::unordered_map is not a bad choice for bank
														account storage, removal, and search.
*/

#include "dataFunctions.hpp"

int main(int argc, char* argv[])
{
	// we need a unordered map to store our key and mapped values
	// std::unordered_map<keyType, ValueType>; What should the key be? What about the value?

	std::unordered_map<int, BankData> bankMap;

	std::ifstream infile;

	infile.open("BankAccounts.csv");

	std::cout << std::endl;

	std::cout << "Insertion: " << std::endl;

	std::cout << std::endl;

	readData(bankMap, infile);

	std::cout << std::endl;

	infile.close();

	std::cout << std::endl;

	std::cout << "After insertion: " << std::endl;

	std::cout << std::endl;

	printData(bankMap);

	std::cout << std::endl;

	bankMap.erase(11111111);

	std::cout << "After deletion: " << std::endl;

	std::cout << std::endl;

	printData(bankMap);

	printNumBuckets(bankMap);

	std::cout << std::endl;
	
	return 0;
}