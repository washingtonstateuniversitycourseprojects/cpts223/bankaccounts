#include "BankData.hpp"

int BankData::getAcctNum() const
{
	return *(this->mpAcctNum);
}

double BankData::getSavingsAmount() const
{
	return *(this->mpSavingsAmount);
}

double BankData::getCheckingAmount() const
{
	return *(this->mpCheckingAmount);
}

void BankData::setAcctNum(const int& newAcctNum)
{
	mpAcctNum = new int;

	*mpAcctNum = newAcctNum;
}

void BankData::setSavingsAmount(const double& newSavingsAmount)
{
	mpSavingsAmount = new double;

	*mpSavingsAmount = newSavingsAmount;
}

void BankData::setCheckingAmount(const double& newCheckingAmount)
{
	mpCheckingAmount = new double;

	*mpCheckingAmount = newCheckingAmount;
}

std::ostream& operator<<(std::ostream& filestream, BankData data)
{
	filestream << "Account number: " << data.getAcctNum() << ", savings: $" << std::setprecision(2) << std::fixed << data.getSavingsAmount() << ", checking: $" << std::setprecision(2) << std::fixed << data.getCheckingAmount();

	return filestream;
}